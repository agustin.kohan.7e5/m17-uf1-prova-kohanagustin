using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFunction : MonoBehaviour
{
    Rigidbody2D _rb;

    public float Speed;
        // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = new Vector2(0, Speed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision);
            Destroy(this.gameObject);
        }
    }
}
