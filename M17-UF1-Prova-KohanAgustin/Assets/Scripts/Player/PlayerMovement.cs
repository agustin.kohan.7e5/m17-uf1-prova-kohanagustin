using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed;
    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
        Move();
    }

    // Vector2 cameraBorders = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

    private void Move()
    {
        _rb.velocity = new Vector2(Input.GetAxis("Horizontal") * Time.deltaTime, Input.GetAxis("Vertical") * Time.deltaTime) * Speed;
        
    }

    // _rb.velocity = (gameObject.GetComponent<PlayerData>().ActualSpeed() * 250) * new Vector2(Input.GetAxis("Horizontal") * Time.deltaTime, 0);
}
