using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightMovement : MonoBehaviour
{
    public float Speed;

    
    Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {

        _rb = GetComponent<Rigidbody2D>(); 
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        _rb.velocity = new Vector2(0, -1 * Time.deltaTime) * Speed;      
    }
}
