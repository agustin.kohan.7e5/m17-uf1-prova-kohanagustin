using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipDie : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            GameObject.Find("PlayerShip").GetComponent<PlayerData>().AddScore(5);
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("M17UF1-Prova");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
