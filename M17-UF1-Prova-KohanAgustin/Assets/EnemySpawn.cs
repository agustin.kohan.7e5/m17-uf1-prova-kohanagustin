using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public enum SpawnerState
    {
        Active,
        Waiting,
        Disabled,
    }

    [SerializeField]
    private GameObject _enemyPrefab;

    public float _spawnTimeLapse;
    private float _cooldownSpawnerTime;



    GameObject _player;

    private SpawnerState _spawnerState;

    // Start is called before the first frame update
    void Start()
    {
        _cooldownSpawnerTime = 0f;
        _player = GameObject.Find("PlayerShip");
    }

    // Update is called once per frame
    void Update()
    {
        if (_spawnerState == SpawnerState.Active)
        {
            SpawnNewEnemy();
            _spawnerState = SpawnerState.Waiting;
        }
        else if (_spawnerState == SpawnerState.Waiting)
        {
            _cooldownSpawnerTime += Time.deltaTime;
            if (_cooldownSpawnerTime >= _spawnTimeLapse)
            {
                _spawnerState = SpawnerState.Active;
                _cooldownSpawnerTime = 0;
            }
        }
    }

    private void SpawnNewEnemy()
    {
        if (_player != null)
        {
            ChangePosition();
            InstantiateFireball();
        }
    }

    private void InstantiateFireball()
    {
        Instantiate(_enemyPrefab, this.transform.position, Quaternion.identity);
    }

    private void ChangePosition()
    {
        transform.position = GenerateRandomPosition();
    }

    private Vector2 GenerateRandomPosition()
    {
        // Vector2 cameraBorders = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        //_distanceFormBorderScreenToPlatform = (cameraBorders.x - _ground.GetComponent<BoxCollider2D>().size.x) / 2;

        //_player.transform.position.x;

        return new Vector2(Random.Range(_player.transform.position.x - 5, _player.transform.position.x + 5), transform.position.y);

    }
}
