using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateShoot : MonoBehaviour
{

    [SerializeField]
    GameObject BulletPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    float timer = 0f;
    // Update is called once per frame
    void Update()
    {
        if (timer >= 1f)
        {
            Instantiate(BulletPrefab, new Vector2(transform.position.x, transform.position.y - 1), Quaternion.identity);
            timer = 0;
        }
        timer += Time.deltaTime;
    }
}
