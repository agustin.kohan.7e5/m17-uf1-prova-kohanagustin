using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScore : MonoBehaviour
{
    [SerializeField]
    Text ScoreText;

    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("PlayerShip");
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.text = player.GetComponent<PlayerData>()._score.ToString();
    }
}
